resource "azurerm_public_ip" "centos" {
  count = var.bastion ? 1 : 0
  name                = "${var.name}-centos-ip"
  location            = var.location
  resource_group_name = var.rg-name
  allocation_method   = "Static"
}

resource "azurerm_network_interface" "centos-nic" {
  name                = "main-nic"
  location            = var.location
  resource_group_name = var.rg-name

  ip_configuration {
    name                          = "${var.name}-internal-ip"
    subnet_id                     = var.subnet_id 
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id = var.bastion ? azurerm_public_ip.centos[0].id : null
  }
}

resource "azurerm_linux_virtual_machine" "box" {
  name                = var.name
  resource_group_name = var.rg-name
  location            = var.location
  size                = "Standard_F2"
  admin_username = var.username

  admin_ssh_key {
    username   = var.username
    public_key = var.ssh_key
  }

  network_interface_ids = [
    azurerm_network_interface.centos-nic.id,
  ]

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "OpenLogic"
    offer     = "CentOS"
    sku       = "7.7"
    version   = "latest"
  }
}
